package Model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Translation {
    private String label_FR;
    private String label_EN;
}
