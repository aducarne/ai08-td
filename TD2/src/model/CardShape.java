package Model;

import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import lombok.Getter;

import java.util.Objects;

@Getter
public class CardShape extends StackPane {
    private TransItem transItem;
    private Text displayedText;
    private Rectangle rectangle;

    public CardShape(TransItem transItem) {
        constructCard(transItem);
        new CardController().initBinding();
    }

    private void constructCard(TransItem transItem) {
        rectangle = new Rectangle(100, 100);
        rectangle.setFill(Color.WHITE);
        this.getChildren().add(rectangle);
        displayedText = new Text(transItem.getLabelEN());
        this.getChildren().add(displayedText);
        this.transItem = transItem;
    }

    private void toggleTranslation() {
        this.displayedText.setText(Objects.equals(this.displayedText.getText(), this.transItem.getLabelEN()) ?
                this.transItem.getLabelFR()
                : this.transItem.getLabelEN()
        );
    }

    private class CardController {
        private void initBinding() {
            rectangle.setOnMouseClicked(mouseEvent -> toggleTranslation());
            displayedText.setOnMouseClicked(mouseEvent -> toggleTranslation());
        }
    }
}
