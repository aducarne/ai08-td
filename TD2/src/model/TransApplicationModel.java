package Model;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import javafx.collections.FXCollections;
import javafx.collections.ObservableSet;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.util.List;

public class TransApplicationModel {
    public ObservableSet<TransItem> transItemsSet;

    public TransApplicationModel() {
        transItemsSet = FXCollections.observableSet();
    }

    public void parseFile(File f) {
        Gson gson = new Gson();
        JsonReader reader = null;
        try {
            reader = new JsonReader(new FileReader(f));
            Type collectionType = new TypeToken<List<Translation>>() {
            }.getType();
            List<Translation> data = gson.fromJson(reader, collectionType);
            data.forEach(t -> transItemsSet.add(new TransItem(t.getLabel_FR(), t.getLabel_EN())));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
