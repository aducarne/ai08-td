package view;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.TilePane;
import javafx.stage.Stage;
import lombok.Getter;

@Getter
public class TransApplicationView extends BorderPane {
    private TilePane cardsTilePane;
    private Button openFileButton;

    public TransApplicationView(Stage stage) {
        initScene(stage);
        initTop();
        initTilePane();
    }

    private void initTilePane() {
        cardsTilePane = new TilePane();
        cardsTilePane.setPadding(new Insets(10, 10, 10, 10));
        cardsTilePane.setHgap(20);
        cardsTilePane.setVgap(30);
        cardsTilePane.setId("tilePane");
        this.setCenter(cardsTilePane);
    }

    private void initTop() {
        ToolBar toolBar = new ToolBar();
        openFileButton = new Button("Ouvrir la liste");
        toolBar.getItems().add(openFileButton);
        this.setTop(toolBar);
    }

    private void initScene(Stage stage) {
        Scene scene = new Scene(this, 600, 400);
        stage.setTitle("Traduction");
        stage.setScene(scene);
        stage.show();
    }
}
