import controller.TransApplicationController;
import javafx.application.Application;
import javafx.stage.Stage;

public class TransApplicationStart extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        TransApplicationController controller = new TransApplicationController(stage);
    }
}
