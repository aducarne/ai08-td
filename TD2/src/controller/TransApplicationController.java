package controller;

import javafx.collections.SetChangeListener;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import model.CardShape;
import model.TransApplicationModel;
import model.TransItem;
import view.TransApplicationView;

import java.io.File;
import java.util.Optional;

public class TransApplicationController {
    private TransApplicationView view;
    private TransApplicationModel model;

    public TransApplicationController(Stage stage) {
        view = new TransApplicationView(stage);
        model = new TransApplicationModel();
        initBinding();
    }

    private void initBinding() {
        SetChangeListener<TransItem> f;
        f = change -> {
            if (change.wasRemoved()) {
                view.getCardsTilePane().getChildren().clear();
            }
            if (change.wasAdded()) {
                view.getCardsTilePane().getChildren().add(new CardShape(change.getElementAdded()));
            }
        };

        model.transItemsSet.addListener(f);

        view.getOpenFileButton().setOnMouseClicked(mouseEvent -> openFileDialog());
    }

    private void addTilePaneItem() {
        view.getCardsTilePane().getChildren().add(new CardShape(new TransItem()));
    }

    private void openFileDialog() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("JSON file", "*.json"));
        fileChooser.setInitialDirectory(new File(System.getProperty("user.dir") + "/src/resource/"));

        Optional.ofNullable(fileChooser.showOpenDialog(new Stage())).ifPresent(file -> model.parseFile(file));
    }

}
