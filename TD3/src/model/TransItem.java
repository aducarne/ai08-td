package model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import lombok.Getter;

public class TransItem {
    @Getter
    private String firstLanguage;

    @Getter
    private String secondLanguage;

    @Getter
    private Integer nbClicked = 0;

    private StringProperty selectedWord = new SimpleStringProperty();

    TransItem(String firstLanguage, String secondLanguage) {
        this.firstLanguage = firstLanguage;
        this.secondLanguage = secondLanguage;
        init();
    }

    StringProperty selectedWordProperty() {
        return selectedWord;
    }

    void toggleTranslation() {
        selectedWord.setValue(selectedWord.getValue().equals(firstLanguage) ? secondLanguage : firstLanguage);
        nbClicked++;
    }

    private void init() {
        selectedWord.setValue(firstLanguage);
    }
}
