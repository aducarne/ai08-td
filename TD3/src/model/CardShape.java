package model;

import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import lombok.Getter;

@Getter
public class CardShape extends StackPane {
    private TransItem item;
    private Text word;
    private Rectangle rectangle;

    public CardShape(TransItem transItem) {
        super();
        this.item = transItem;
        constructCard(transItem);
        new CardController();
    }

    private void constructCard(TransItem transItem) {
        rectangle = new Rectangle(200, 100);
        rectangle.setFill(Color.WHITE);
        word = new Text(transItem.getFirstLanguage());
        getChildren().addAll(rectangle, word);
    }

    private class CardController {
        CardController() {
            item.selectedWordProperty().addListener(((observableValue, prevValue, newValue) -> word.setText(newValue)));
            rectangle.setOnMouseClicked(mouseEvent -> item.toggleTranslation());
            word.setOnMouseClicked(mouseEvent -> item.toggleTranslation());
        }
    }
}
