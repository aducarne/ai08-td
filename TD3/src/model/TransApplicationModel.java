package model;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableSet;
import javafx.scene.chart.XYChart;
import lombok.Getter;

import java.io.File;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

@Getter
public class TransApplicationModel {
    private ObservableSet<TransItem> transItemsSet;
    private ObjectProperty<XYChart.Series<String, Number>> seriesData = new SimpleObjectProperty<>();

    private Consumer<XYChart.Series<String, Number>> populateChartData = series -> transItemsSet.forEach(
            item -> series.getData()
                    .add(new XYChart.Data<String, Number>(item.getFirstLanguage(), item.getNbClicked())));

    private Consumer<List<TranslationJSON>> addToItemSet = tList -> tList.forEach(
            t -> transItemsSet.add(new TransItem(t.getFirstLanguage(), t.getSecondLanguage()))
    );

    public TransApplicationModel() {
        transItemsSet = FXCollections.observableSet();
    }

    public void updateSeries() {
        XYChart.Series<String, Number> newSeries = new XYChart.Series<>();
        populateChartData.accept(newSeries);
        seriesData.setValue(newSeries);
    }

    public void populateFromFile(File f) {
        Optional.ofNullable(parseFile(f)).ifPresent(
                tList -> addToItemSet.accept(tList)
        );
    }

    private List<TranslationJSON> parseFile(File f) {
        List<TranslationJSON> result = null;
        try {
            Gson gson = new Gson();
            JsonReader reader = new JsonReader(new FileReader(f));
            Type collectionType = new TypeToken<List<TranslationJSON>>() {
            }.getType();
            result = gson.fromJson(reader, collectionType);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
