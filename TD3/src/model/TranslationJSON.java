package model;

import lombok.Getter;

@Getter
class TranslationJSON {
    private String firstLanguage;
    private String secondLanguage;
}
