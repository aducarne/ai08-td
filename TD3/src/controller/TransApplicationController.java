package controller;

import javafx.collections.SetChangeListener;
import javafx.fxml.FXML;
import javafx.scene.chart.BarChart;
import javafx.scene.control.Tab;
import javafx.scene.layout.TilePane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import model.CardShape;
import model.TransApplicationModel;
import model.TransItem;

import java.io.File;
import java.util.Optional;


public class TransApplicationController {
    @FXML
    Tab graphTab;
    @FXML
    BarChart<String, Number> stats;
    @FXML
    TilePane cards;

    private TransApplicationModel model = new TransApplicationModel();

    public TransApplicationController() {
        initialize();
    }

    private void initialize() {
        SetChangeListener<TransItem> itemChanged = change -> {
            if (change.wasRemoved()) {
                cards.getChildren().clear();
            } else {
                cards.getChildren().add(new CardShape(change.getElementAdded()));
            }
        };

        model.getTransItemsSet().addListener(itemChanged);

        model.getSeriesData().addListener((obsv, oldv, newv) -> {
            stats.getData().clear();
            stats.getData().add(newv);
        });
    }

    public void computeStats() {
        if (graphTab.isSelected()) {
            model.updateSeries();
        }
    }

    public void openFileDialog() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("JSON file", "*.json"));
        fileChooser.setInitialDirectory(new File(System.getProperty("user.dir") + "/src/resources/"));

        Optional.ofNullable(fileChooser.showOpenDialog(new Stage())).ifPresent(file -> {
                    model.getTransItemsSet().clear();
                    model.populateFromFile(file);
                    model.updateSeries();
                }
        );
    }
}
