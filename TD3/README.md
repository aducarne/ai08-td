# Projet réalisé sur Intellij et Java 12

### Dépendences nécessaires

-   Java 12 SDK
-   Lombok 1.8.10 (inclus)
-   GSON 2.8.5 (inclus)
-   Java FX > 11

Afin de faire fonctionner l'application, il faut ajouter toutes les librairies incluent dans le dossier "dependencies" à la racine du projet. De plus, il faut ajouter les options : "--module-path "C:\Program Files\Java\javafx-sdk-13\lib"--add-modules=javafx.controls,javafx.fxml" à la VM Java.

---

Lombok permet de réduire la verbosité du code en générant automatiquement du code lors de la compilation à l'aide d'annotations. Ici, il est uniquement utilisé pour créer des Getter et des Setter sur certaines propriétés de classe. Ainsi, l'annotation @Getter créera un getter sur la propriété tel que ci-après : public <T> getX(){return x}

---

J'ai préféré utiliser un fichier JSON pour importer les traductions. De ce fait, j'ai utilisé la librairie google GSON afin de déserialiser le tableau de traductions en liste d'objets Java TranslationJSON.

---

Enfin, j'ai modifié la couleur de fond du graphe à l'aide d'un fichier CSS. De plus, j'ai également arrondi les bords de celui-ci.
